# Collaboratorium
## A collaboration platform created by Alexander Glavusanov and Daniel Ninov

### Check out a working demo [here](https://collaboratorium-edc48.web.app/home)

## Features

### Private and public boards

Toggle between public and private states on your projects. Custom sharing options are also available - invite your teammates to collaborate using their registered email address.

### Real-time collaboration

State is saved and updated in real time among users. 

### Whiteboard

Draw using a pencil, straight lines or figures. Import images, insert text, zoom in/out and more. State saving is performed on demand.

### Kanban Board

Design your own Kanban board - you can add/remove cards and columns, edit their titles, descriptions, even coloring. Supports assigning cards to people and setting start/end dates. All changes are performed in real time.

### Basic user management

Firebase allows users to register with an email address and set usernames. Authentication and password reset and recovery supported.

### Chat

Chat with your teammates while working on the same board.

### Pointer sharing

Share your pointer's position with your teammates. 

---

## Run it yourself

### Host it locally

Collaboratorium uses Firebase services for authentication, state update and more.

In order to use it to its full potential, you need to have a Firebase project.

You can create one [here](https://firebase.google.com/).

After creating your project, initialize Firebase by entering the config information found in src/index.js with your project's config data. Setup process for Node.js environment outlined in detail [here](https://firebase.google.com/docs/web/setup#node.js-apps).

Run the following command to automatically download and install dependencies:

    npm install

Then, to host locally, use

    npm run start

---

## Host it on Firebase

Hosting the project requires installing the Firebase CLI, initialising your project and deploying the code. The steps depend on your setup. Detailed walkthrough can be found [here](https://firebase.google.com/docs/hosting/quickstart).

### Using npm:

First, install the Firebase CLI.

    npm install -g firebase-tools

Then, log in.

    firebase login

Your next step is to initialize the project.

    firebase init

Finally, deploy.

    firebase deploy

---

## User guide

This guide is aimed at anyone who wishes to use this application's services.

---

### Notes

The application does not support viewports narrower than 800 px.

---

### Overview

Collaboratorium offers a lightweight collaboration service for planning, brainstorming and any other team-based activity that requires writing stuff down.

The main elements are the different types of boards - whiteboards and Kanban boards.

Users can work by themselves or with others - Collaboratorium supports input from different clients on a basic level, which means that changes in state are applied in real time.

---

### Registration

Registering requires an email address, a desired username and password. Form validation is applied, so if the "Register" button is disabled, make sure that the input is valid.


---

### Log in

After registering, users are required to log in. All functionalities are only available to logged users.

Password recovery is supported.

---

### Viewing public boards

Once logged in, users have access to the full navigation bar. In order to view all public whiteboards, please select `Public Drawings`. `Public Boards` leads to all public Kanban boards.

On all non-owned whiteboards and Kanbans, only the option to `View` will be visible.

On owned whiteboards, the options to `Rename`, `Delete`, `Toggle Publicity`, `Invite` and `View` are available.

On all owned Kanbans, we can `Rename`, `Delete`, `Toggle Publicity` and `View`.

---


### My Whiteboards and My Boards

Two types of boards appear here:
+ private boards, created by the logged user
+ private boards that have been shared with the logged user

---

### Creating boards

Users can create whiteboards from the `Public Drawings` and `My Drawings` pages.  
Kanban boards are created from `Public Kanbans` and `My Kanbans`.


---


### Working with the whiteboard

Available tools are as follows:

- `Select` - Select elements via clicking or clicking and dragging
- `Pan` - Pan viewport
- `Pencil` - Draw anything
- `Line` - Draw straight lines by clicking and dragging
- `Rectangle` - Allows the user to draw a rectangle by clicking and dragging
- `Circle` - Insert a circle. Click and drag to increase/decrease size
- `Add text` - Users can modify the text they want to add in the box just over the button
- `Add image` - Users can add images by URL. URL is to be entered in the text field

Modify existing content with:

- `Clear` - clears the whole whiteboard. Use with caution, as it is irreversible
- `Undo` - Undos the last performed action. Does not work with Clear
- `Redo` - Reverses performed Undos
- `Save` - saves changes to the database
- `Zoom` In/Out - Zooms in and out
- `Copy` - Copies selection
- `Paste` - Pastes last copied object
- `Erase` - Erases selected object


Other options:

- `Import Kanban` - Imports a PNG render of the last uploaded Kanban. Kanbans are uploaded from the respective Kanban view pages
- `Go to Kanban` - opens a dedicated view of the last uploaded Kanban


`Line width` and `Color` affect all drawing tools. Color is shared among collaborators.


Keyboard hotkeys are also implemented. Tutorial can be found on the whiteboard page itself.


---

### Working with the Kanban Board

**Add** columns and cards by clicking the buttons below the Kanban window.

**Remove** columns and cards by clicking the `X` next to their title.

**Rename** columns and cards by clicking on their title.

**Move** columns and cards by dragging.

Open the *Edit Card* modal by clicking the `⌘` button on each card. In it, **edit** card description, **set** Due/Start dates and **assign** the card to a user.

**Add/remove colors** to the board by selecting `Show Board Colors`. Apply colors to columns by clicking their name. Apply colors to cards by opening the *Edit Card* modal.

**Invite** collaborators with the `Invite` button and their registered e-mail address.

**Upload** the board to the database as a PNG with `Upload Board as PNG`. The last uploaded board by any user can be imported in *Whiteboards*.

---

### Chat

The chat box is opened by clicking the `Chat icon` in the bottom right corner.

Chat rooms are linked to board IDs.

Messages sent by other users show their display name.

Chat messages are only received after entering the room. History is not shown.

---

### Pointer sharing

Start sharing your pointer position with collaborators on the same board by clicking `Pointer Sharing On`.

Stop sharing by selecting `Pointer Sharing Off`.

---

## Project Planning Board

See our development process plan here:
https://trello.com/b/g4y0CR1V/app-design