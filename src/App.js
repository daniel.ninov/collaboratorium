import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Home from './components/Pages/Home/Home';
import Register from './components/User/Register/Register';
import SignIn from './components/User/SignIn/SignIn';
import Container from './components/Base/Container/Container';
import AuthContext from './providers/AuthContext';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import Navigation from './components/Base/Navigation/Navigation';
import { useAuthState } from 'react-firebase-hooks/auth';
import PublicBoardsPage from './components/Kanban/PublicBoardsPage/PublicBoardsPage';
import PersonalBoardsPage from './components/Kanban/PersonalBoardsPage/PersonalBoardsPage';
import PublicWhiteboards from './components/Whiteboard/PublicWhiteboards/PublicWhiteboards';
import PersonalWhiteboards from './components/Whiteboard/PersonalWhiteboards/PersonalWhiteboards';
import GuardedRoute from './providers/GuardedRoute';
import Profile from './components/User/Profile/Profile';
import ResetPassword from './components/User/ResetPassword/ResetPassword';
import DisplayKanbanBoardDetails from './components/Kanban/DisplayKanbanBoardDetails/DisplayKanbanBoardDetails';
import WhiteboardCursors from './components/Whiteboard/WhiteboardCursors/WhiteboardCursors';

function App() {
  const [user, loading] = useAuthState(firebase.auth());

  if(loading) {
    return <h1>LOADING</h1>;
  }

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ currentUser: user }}>
        <Navigation />
        <Container>
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Home} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={SignIn} />
            <Route path='/reset' component={ResetPassword} />
            <GuardedRoute path='/boards' auth={!!user} exact component={PublicBoardsPage} />
            <GuardedRoute path='/boards/:id' auth={!!user} component={DisplayKanbanBoardDetails} />
            <GuardedRoute path='/myboards' auth={!!user} component={PersonalBoardsPage} />
            <GuardedRoute path='/drawings' auth={!!user} exact component={PublicWhiteboards} />
            <GuardedRoute path='/mydrawings' auth={!!user} exact component={PersonalWhiteboards} />
            <GuardedRoute path='/draw/:id' auth={!!user} component={WhiteboardCursors} />
            <GuardedRoute path='/profile' auth={!!user} component={Profile} />
          </Switch>
        </Container>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
