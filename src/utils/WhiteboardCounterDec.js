import * as firebase from 'firebase/app';
import 'firebase/auth';


const WhiteboardCounterDec = async (isPublic) => {

  let privateBoardsCounter;
  let publicBoardsCounter;

  const database = firebase.database();

  await database.ref('whiteboardsCounters/privateBoardsCounter').once('value')
    .then((res) => privateBoardsCounter = +res.val())
    .catch(e => console.log(e.message));

  await database.ref('whiteboardsCounters/publicBoardsCounter').once('value')
    .then((res) => publicBoardsCounter = +res.val())
    .catch(e => console.log(e.message));

  if (isPublic) {
    publicBoardsCounter--;
    await database.ref('whiteboardsCounters/publicBoardsCounter').set(publicBoardsCounter);
  } else {
    privateBoardsCounter--;
    await database.ref('whiteboardsCounters/privateBoardsCounter').set(privateBoardsCounter);
  }
};
export default WhiteboardCounterDec;