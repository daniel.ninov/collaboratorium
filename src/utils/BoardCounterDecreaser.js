import * as firebase from 'firebase/app';
import 'firebase/auth';


const BoardCounterDecreaser = async (isPublic) => {

  let privateBoardsCounter;
  let publicBoardsCounter;

  const database = firebase.database();

  await database.ref('boardsCounters/privateBoardsCounter').once('value')
    .then((res) => privateBoardsCounter = +res.val())
    .catch(e => console.log(e.message));

  await database.ref('boardsCounters/publicBoardsCounter').once('value')
    .then((res) => publicBoardsCounter = +res.val())
    .catch(e => console.log(e.message));

  if (isPublic) {
    publicBoardsCounter--;
    await database.ref('boardsCounters/publicBoardsCounter').set(publicBoardsCounter);
  } else {
    privateBoardsCounter--;
    await database.ref('boardsCounters/privateBoardsCounter').set(privateBoardsCounter);
  }
};
export default BoardCounterDecreaser;