const turnOffListeners = () => {
  window.onmousemove = () => {};
  window.onmouseout = () => {};
}

export default turnOffListeners;