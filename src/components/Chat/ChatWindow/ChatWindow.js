import React, { useContext, useEffect, useState } from 'react';
import { Widget, addResponseMessage, deleteMessages, setBadgeCount } from 'react-chat-widget';
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import 'react-chat-widget/lib/styles.css';
import AuthContext from '../../../providers/AuthContext';


const ChatWindow = ({ boardId, boardTitle, room }) => {

  const database = firebase.database();
  const chatRoomRef = database.ref('chats/' + room + boardId);

  const authContext = useContext(AuthContext);
  const user = authContext.currentUser;
  const userDisplayName = user.displayName;

  const [chatMessages, setChatMessages] = useState([]);

  useEffect(() => {

    let logged = false;
    firebase.database().ref('chats/' + room + boardId).on('value', snapshot => {

      const newChatMessages = snapshot.val();
      if (!newChatMessages) {

        const chatInitializationArray = [
          {
            value: "This is the first message in this board's chat.",
            number: 1,
            senderDisplayName: 'System',
          }
        ];

        firebase.database().ref('chats/' + room + boardId).set(chatInitializationArray);
      } else {

        const newChatMessage = newChatMessages[0];
        setChatMessages(newChatMessages);

        if (!logged) {
          addResponseMessage(`Welcome to the chat room for ${boardTitle}`);
          deleteMessages(999);
          setBadgeCount(1);
        }
        if (logged && newChatMessage.senderDisplayName !== userDisplayName) {
          const responseMessageToBeAdded = `${newChatMessage.senderDisplayName} says:\n\r${snapshot.val()[0].value}`;

          addResponseMessage(responseMessageToBeAdded);
        }

        logged = true;

      }
    })

    return () => {
      firebase.database().ref('chats/' + room + boardId).off();
      deleteMessages(99999);
    };
  }, [userDisplayName, room, boardId, boardTitle]);


  const handleNewUserMessage = (newMessage) => {

    const messageToSend = {
      value: newMessage,
      number: chatMessages.length + 1,
      senderDisplayName: user.displayName,
    }

    const updatedChatMessages = [...chatMessages];
    updatedChatMessages.unshift(messageToSend);

    chatRoomRef.set(updatedChatMessages);
  };


  return (
    <>
      <Widget
        handleNewUserMessage={handleNewUserMessage}
        subtitle={null}
        title={null}
      />
    </>
  )
};

export default ChatWindow;
