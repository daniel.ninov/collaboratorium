import React, { useEffect, useCallback, useState } from 'react';
import { CustomCursor } from './CustomCursor';
import { throttle } from '../../../common/utils';
import { Button } from 'react-bootstrap';
import turnOffMouseListeners from '../../../utils/TurnOffMouseListeners';
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';


const Cursors = ({ instance, roomRef }) => {
  const [cursors, setCursors] = useState({});


  const throttlePositionChange = useCallback(
    throttle(instance?.onCursorPositionChanged, 20),
    [instance]
  );

  const positionChangeNoThrottle = useCallback(
    throttle(instance?.onCursorPositionChanged, 0), [instance]
  );

  const onMouseMove = (e, visible) => {
    throttlePositionChange({ x: e.clientX, y: e.clientY, visible: visible });
  };

  useEffect(() => {

    const handleCursor = (event, userId, posData) => {
      if (!userId) {
        return;
      }

      switch (event) {
        case 'add':
          setCursors(prevCursors => {
            const copiedCursors = { ...prevCursors };
            copiedCursors[userId] = { ...posData };
            return copiedCursors;
          });
          break;
        case 'change':
          setCursors(prevCursors => {
            const copiedCursors = { ...prevCursors };
            copiedCursors[userId] = { ...posData };
            return copiedCursors;
          });
          break;
        case 'remove':
          setCursors(prevCursors => {
            const copiedCursors = { ...prevCursors };
            const { [userId]: cursorToDelete, ...restCursors } = copiedCursors;
            return restCursors;
          });
          break;
        default:
          break;
      }
    };

    instance.monitorCursors(handleCursor);

    return () => {
      firebase.database().ref('cursors/').child(`${roomRef}`).off();
      instance.disconnect();
      turnOffMouseListeners();
    };
  }, [instance, roomRef]);

  const mouseMoveListenersOn = () => {
    window.onmousemove = (mouseEvent) => {
      onMouseMove(mouseEvent, true);
    };

    window.onmouseout = () => {
      positionChangeNoThrottle({ x: 0, y: 0, visible: false });
    }

    throttlePositionChange({ x: 0, y: 0, visible: true });
  }

  const clearCursor = () => {
    positionChangeNoThrottle({ x: 0, y: 0, visible: false });
  }

  return (
    <div>
      <Button onClick={mouseMoveListenersOn} style={{ marginRight: 10 }}>Pointer sharing on</Button>
      <Button style={{ paddingLeft: 22, paddingRight: 21 }} onClick={() => {
        turnOffMouseListeners();
        clearCursor();
      }}>Pointer sharing off</Button>
      <div className="bring-front" style={{
        position: 'fixed',
        left: '0',
        top: '0',
      }}>
        {Object.values(cursors).map(cursor => (
          <CustomCursor
            key={cursor.id}
            visible={cursor.visible}
            id={cursor.id}
            x={cursor.x}
            y={cursor.y}
          />
        ))}

      </div>
    </div>
  );
};

export default Cursors;