import React, { useEffect, useState } from 'react';
import { COLORS } from '../../../common/utils';
import { motion, useMotionValue } from 'framer-motion';

const CURSOR_SIZE = 40;
const FRUITS = [
  { fruit: '🍎', color: COLORS.red },
  { fruit: '🍓', color: COLORS.red },
  { fruit: '🍉', color: COLORS.pink },
  { fruit: '🍇', color: COLORS.purple },
  { fruit: '🍑', color: COLORS.orange },
  { fruit: '🍊', color: COLORS.orange },
  { fruit: '🍋', color: COLORS.yellow },
  { fruit: '🍌', color: COLORS.yellow },
  { fruit: '🍏', color: COLORS.green },
  { fruit: '🍍', color: COLORS.blue }
];

const getFruit = id => {
  const index = (id?.charCodeAt(0) || 0) % FRUITS.length;
  return FRUITS[index];
};

export const CustomCursor = ({ id, x, y, visible } = { id: '0', x: 0, y: 0, visible: false}) => {
  const posX = useMotionValue(0);
  const posY = useMotionValue(0);
  const [visibleState, setVisibleState] = useState(false);

  useEffect(() => {
    posX.set(x);
  }, [x, posX]);

  useEffect(() => {
    posY.set(y - CURSOR_SIZE);
  }, [y, posY]);

  useEffect(() => {
    setVisibleState(visible);
  }, [visible, visibleState])

  if (visibleState === false) return null;

  return (
    <motion.div
      style={{
        y: posY,
        x: posX,
        width: CURSOR_SIZE,
        height: CURSOR_SIZE,
        position: 'absolute',
        fontSize: 28,
        userSelect: 'none',
        background: getFruit(id).color.dim,
        boxShadow: `inset 0px 0px 0px 2px ${
          getFruit(id).color.default
        }, 0px 8px 16px rgba(0,0,0,0.4)`,
        borderRadius: '50%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: '10',
        pointerEvents: 'none',
      }}
    >
      {getFruit(id).fruit}
      <svg
        style={{
          position: 'absolute',
          bottom: 0,
          left: 0,
          zIndex: '10',
          pointerEvents: 'none',
        }}
        width="12"
        height="12"
        viewBox="0 0 12 12"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M2 12H9.17157C10.9534 12 11.8457 9.84572 10.5858 8.58579L3.41421 1.41421C2.15428 0.154281 0 1.04662 0 2.82843V10C0 11.1046 0.89543 12 2 12Z"
          fill={getFruit(id).color.default}
        />
      </svg>
    </motion.div>
  );
};