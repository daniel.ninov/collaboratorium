import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

class CursorHandler {
  database;
  currentUserId;

  constructor(roomId) {
    this.roomRef = roomId; 
  }

  onCursorPositionChanged = ({ x, y, visible }) => {
    if(!this.currentUserId) {
      return;
    }
    firebase.database().ref('cursors/').child(`${this.roomRef}`)
      .child(this.currentUserId)
      .set({ id: this.currentUserId, x, y, visible });

    firebase.database().ref('cursors/' + this.roomRef)
      .child(this.currentUserId)
      .onDisconnect()
      .remove();
  };

  monitorCursors = cb => {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        var uid = user.uid;

        this.currentUserId = uid;
        this._monitorCursors(cb);
      }
    });
  };

  _monitorCursors = cb => {
    firebase.database().ref('cursors/').child(`${this.roomRef}`).once('value', snap => {
      snap.forEach(item => {
        if (item.key === this.currentUserId) {
          return;
        }
        cb('add', item.key, item.val());
      });
    });

    firebase.database().ref('cursors/').child(`${this.roomRef}`).on('child_added', snap => {
      if (snap.key === this.currentUserId) {
        return;
      }

      cb('add', snap.key, snap.val());
    });
    firebase.database().ref('cursors/').child(`${this.roomRef}`).on('child_changed', snap => {
      if (snap.key === this.currentUserId) {
        return;
      }

      cb('change', snap.key, snap.val());
    });

    firebase.database().ref('cursors/').child(`${this.roomRef}`).on('child_removed', snap => {
      cb('remove', snap.key);
    });
  };

  disconnect = () => {
    firebase.database().ref('cursors/').child(`${this.roomRef}`).off();
    firebase.database().ref(`cursors/${this.roomRef}/` + this.currentUserId).remove();
  };
}

const getCursorHandlerInstance = (roomId) => {
  return new CursorHandler(roomId);
};

export { getCursorHandlerInstance };