import React from 'react';
import Container from '../Container/Container';

const Error = ({ message, setError }) => {
    const clearError = () => setError(false);

    return (
        <Container>
            <h1>{message}</h1>
            <button className="btn btn-primary" onClick={clearError}>Alright</button>
        </Container>
    );
};

export default Error;