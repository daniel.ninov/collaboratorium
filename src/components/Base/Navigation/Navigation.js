import React, { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import Container from '../Container/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    const { currentUser } = useContext(AuthContext);

    const logout = () => {
        localStorage.removeItem('user');
        firebase.auth().signOut().then(result => {
            if (result.error) {
                throw new Error(result.message);
            }

        }).catch(error => console.log(error.message));
    };

    return (
        <Container>
            <Navbar>
                <NavLink className="navbar-brand" to="/home">Collaboratorium</NavLink>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {currentUser
                            ?
                            (
                                <>
                                    <NavLink className="nav-link" to="/profile">Hello, {currentUser.providerData[0].displayName}</NavLink>
                                    <NavLink className="nav-link" to="/boards">Public Kanbans</NavLink>
                                    <NavLink className='nav-link' to='/myboards'>My Kanbans</NavLink>
                                    <NavLink className="nav-link" to="/drawings">Public Whiteboards</NavLink>
                                    <NavLink className='nav-link' to='/mydrawings'>My Whiteboards</NavLink>
                                    <Nav.Link href="/home" onClick={logout}>Sign Out</Nav.Link>
                                </>
                            ) : (
                                <>
                                    <NavLink className="nav-link" to="/login">Sign In</NavLink>
                                    <NavLink className="nav-link" to="/register">Register</NavLink>
                                </>
                            )}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </Container>
    );
};

export default Navigation;