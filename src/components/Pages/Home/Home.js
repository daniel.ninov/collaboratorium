import React from 'react';
import Container from '../../Base/Container/Container';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

const Home = ({history}) => {

  const goToLogIn = () => {
    history.push('/login');
  };

  const goToRegister = () => {
    history.push('/register');
  };

  const goToPublicKanbanBoards = () => {
    history.push('/boards');
  };

  const goToPublicDrawings = () => {
    history.push('/drawings');
  };


  const [user] = useAuthState(firebase.auth());


  return (
    <Container>
      <div className="jumbotron">
        <h1 className="display-3">Collaborate. Creatively.</h1>
        <img src='https://i.imgur.com/Tew7NTt.png' alt="circle" style={{opacity: 0.15, position: 'absolute', zIndex: '5', pointerEvents: 'none', width: '600px', height: 'auto', top: '10%', right: '10%'}}/>
        <p className="lead">Collaboratorium makes collective planning a breeze.</p>
        <hr className="my-4" />
        
          {(user === null) ?
          <p className="lead button-holder">
          <button className="btn btn-primary" onClick={goToRegister}>Sign up</button>
          <button className="btn btn-primary" style={{zIndex: '10'}} onClick={goToLogIn}>Log in</button>
          </p>
          :
          <p className="lead button-holder">
          <button className="btn btn-primary" onClick={goToPublicDrawings}>Whiteboards</button>
          <button className="btn btn-primary" style={{zIndex: '10'}} onClick={goToPublicKanbanBoards}>Kanban</button>
          </p>
          }
        
        <hr className="my-4" />
        <h4>Public, private or invite-only</h4>
        <p className='lead'>Contribute on any public project or work in solitude on private one. Invite your teammates for a quick brainstorming session on any board, at any time.</p>
        
        <h4>Whiteboard</h4>
        <p className='lead'>Quickly sketch down an idea or work with colleagues to bring a plan to life. Draw, import text and images, zoom out, pan around and explore your creative side.</p>

        <h4>Kanban board</h4>
        <p className='lead'>Track your progress with an interactive Kanban board with real-time updates from collaborators. Assign cards, colors, start and end dates and import your Kanban onto a whiteboard.</p>

        <h4>Collaboration tools</h4>
        <p className='lead'>Communicate via inbuilt chat. Share your pointer's position with your teammates.</p>
        
        <p className='lead'></p>
      </div>

    </Container>
  );
};

export default Home;