import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { EMAIL_REGEX } from '../../../common/constraints';
import * as firebase from 'firebase/app';
import 'firebase/database';

const KanbanBoardInvite = ({ boardId }) => {
  const [user, setUser] = useState({
    email: {
      value: '',
      touched: false,
      valid: true,
    }
  });

  const userValidators = {
    email: [
      value => value && value.length >= 3,
      value => value && EMAIL_REGEX.test(value),
    ]
  };

  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && validatorFn(value), true),
    }
  });

  const getClassNames = (prop) => {
    let classes = '';

    if(!user[prop].valid) {
      classes += ' invalid';
    }

    return classes;
  };

  const validateForm = () => !Object
  .keys(user)
  .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

  const [show, setShow] = useState(false);
  const [invalid, setInvalid] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleSubmit = (e) => {
    e.preventDefault();

    inviteUser();
    setShow(false);
  };

  const inviteUser = () => {
   firebase.database().ref('boards/' + boardId).once('value')
   .then(snapshot => {
     const boardVal = snapshot.val();

     firebase.database().ref('users/').once('value')
     .then(users => {
      const usersVal = users.val();
      const userIdToInvite = Object.keys(usersVal).filter(key => usersVal[key].email === user.email.value);
      
      if(userIdToInvite.length < 1) {
        setInvalid(true);
        return null;
      }

      if(boardVal.details.allowedIds.includes(userIdToInvite[0])) {
        setInvalid(true);
        return null;
      }

      boardVal.details.allowedIds.push(userIdToInvite[0]);
      
      firebase.database().ref('boards/' + boardId).set(boardVal);
     })
   });

  };

  const renderModal = () => {
    return (
      <>
        <Button onClick={handleShow}>
          Invite
        </Button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Invite User</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <div className="form-group row">
                <label htmlFor="inputEmail" className="col-sm-2 col-form-label">
                  Email
                </label>
                <div className="col-sm-10" style={{ marginTop: 7 }}>
                  <input type="email" id="inputEmail"
                  className={getClassNames('email')}
                  onChange={e => updateUser('email', e.target.value)} 
                  value={user.email.value} />
                </div>
              </div>
              <div className="form-group row float-right">
                <div className="col-auto">
                  <button type="submit" className="btn btn-outline-primary"
                  onClick={e => handleSubmit(e)} disabled={validateForm()}>Invite</button>
                </div>
              </div>
            </form>
          </Modal.Body>
        </Modal>

        <Modal show={invalid} onHide={() => setInvalid(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Invalid User</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row col-sm-10">User with this email does not exist or is already invited.</div>
          </Modal.Body>
          <Modal.Footer>
            <button className="btn btn-outline-primary" onClick={() => setInvalid(false)}>Alright</button>
          </Modal.Footer>
        </Modal>
      </>
    );
  };

  return renderModal();
};

export default KanbanBoardInvite;