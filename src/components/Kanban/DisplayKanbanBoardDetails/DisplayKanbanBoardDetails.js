import React from 'react';
import { getCursorHandlerInstance } from '../../Collaboration/Cursors/CursorHandler';
import Cursors from '../../Collaboration/Cursors/Cursors';
import KanbanBoard from '../KanbanBoard/KanbanBoard';

const KanbanBoardWithCursors = ({match, history}) => {

  return (
    <div>
      <KanbanBoard />
      <Cursors
      roomRef={match.params.id}
      instance ={getCursorHandlerInstance && getCursorHandlerInstance(match.params.id)}
      />
    </div>
  )
}

export default KanbanBoardWithCursors;