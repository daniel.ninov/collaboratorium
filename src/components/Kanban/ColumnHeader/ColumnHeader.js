import React, { useState } from 'react'
import { Dropdown } from 'react-bootstrap'

function ColumnTitle({ allowRenameColumn, onClick, children: title }) {
  return allowRenameColumn ? (
    <span style={{ cursor: 'pointer' }} onClick={onClick}>
      {title}
    </span>
  ) : (
      <span>{title}</span>
    )
}

function useRenameMode(state) {
  const [renameMode, setRenameMode] = useState(state)

  function toggleRenameMode() {
    setRenameMode(!renameMode)
  }

  return [renameMode, toggleRenameMode]
}

export default function ColumnHeader({ passedColumn, allowRemoveColumn, onColumnRemove, allowRenameColumn, onColumnRename, colors, handleColumnColorChange }) {
  const [renameMode, toggleRenameMode] = useRenameMode(false)
  const [titleInput, setTitleInput] = useState('')

  function handleRenameColumn(event) {
    event.preventDefault()

    onColumnRename(passedColumn, titleInput)
    toggleRenameMode()
  }

  function handleRenameMode() {
    setTitleInput(passedColumn.title)
    toggleRenameMode()
  }

  function colorChanger() {
    return (
      <Dropdown className="float-right" drop='right'>
        <Dropdown.Toggle style={{
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: 5,
          paddingRight: 5,
          marginLeft: 5,
          }} id='dropdown-basic'>Colors</Dropdown.Toggle>

        <Dropdown.Menu>
          {colors.map(color => {
            return <Dropdown.Item key={color} id={color} as='button' onClick={e => handleColumnColorChange({ ...passedColumn, color: e.target.id })} style={{ backgroundColor: color, margin: '5px', height: '20px', width: '20px' }} />
          })}

          <Dropdown.Item id='remove-color' as='button' onClick={e => handleColumnColorChange({ ...passedColumn, color: null })}>Remove color</Dropdown.Item>
        </Dropdown.Menu>

      </Dropdown>
    )
  }

  return (
    <div className='react-kanban-column-header'>
      {renameMode ? (
        <form onSubmit={handleRenameColumn} style={{
          display: 'inline-flex',
          alignItems: 'center',
        }}>
          <span>
            <input
              style={{
                display: 'inline-flex',
                alignItems: 'center',
                minWidth: 120,
                maxWidth: 120,
                fontSize: 17,
                marginRight: 10,
              }}
              type='text'
              value={titleInput}
              onChange={({ target: { value } }) => setTitleInput(value)}
              autoFocus
            />
          </span>
          <span>
            <span style={{ cursor: 'pointer', fontSize: 35 }} type='submit'>
              √
            </span>
            <span style={{ cursor: 'pointer', fontSize: 35 }} type='button' onClick={handleRenameMode}>
              ×
            </span>
          </span>
          {colorChanger()}
        </form>
      ) : (
          <div style={{ display: 'flex' }}>
            {passedColumn.color ?
              <div style={{ backgroundColor: passedColumn.color, width: '20px', height: '20px' }} /> :
              null}
            <ColumnTitle allowRenameColumn={allowRenameColumn} onClick={handleRenameMode}>
              {passedColumn.title}
            </ColumnTitle>
            {allowRemoveColumn && <span style={{ cursor: 'pointer' }} onClick={() => onColumnRemove(passedColumn)}>×</span>}
          </div>
        )}
    </div>
  )
}