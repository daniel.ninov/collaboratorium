import React, { useState, useEffect, createRef } from 'react';
import { TwitterPicker } from 'react-color';
import { Button } from 'react-bootstrap';
import DisplaySingleColorOption from '../DisplaySingleColorOption/DisplaySingleColorOption';

const ColorOptions = ({ boardColors, removeColor, addColor }) => {
  const [colors, setColors] = useState([]);
  const [showAddColorMenu, setShowAddColorMenu] = useState(false);
  const [colorCode, setColorCode] = useState('');
  const [showColorPicker, setShowColorPicker] = useState(false);

  const colorPickerPackageRef = createRef();

  let timeOutId = null;
  const onBlurHandler = () => {
    timeOutId = setTimeout(() => {
      setShowColorPicker(false);
    }, 200);
  };
  const onFocusHandler = () => {
    clearTimeout(timeOutId);
  }

  const toggleShowAddColorMenu = () => setShowAddColorMenu(!showAddColorMenu);
  const toggleShowColorPicker = () => setShowColorPicker(!showColorPicker);

  useEffect(() => {
    setColors(boardColors);
  }, [boardColors]);

  const displayCurrentColors = () => {
    return (
      <div style={{ marginTop: 10 }}>
        {colors ? null :
          'No colors have been added. You might want to add one.'}
        {colors?.map((e, i) => {
          if (e === 0) return null;

          return (
            <DisplaySingleColorOption
              color={e}
              key={i}
              removeColor={removeColor}
              addNewColor={null}
              colorId={i}
            />
          )
        })}
      </div>
    )
  }

  const handleAddColor = () => {
    addColor(colorCode.hex);
    setColorCode('');
    toggleShowColorPicker();
  }

  const AddColorPicker = React.forwardRef((props, ref) => {
    return (
      <div onClick={() => handleClick()}>
        <TwitterPicker
          color={colorCode ? colorCode : '#fff'}
          onChangeComplete={setColorCode}
        />
        <Button style={{ marginTop: 5 }} onClick={() => handleAddColor()}>Add color</Button>
        <Button style={{ marginTop: 5 }} ref={ref} variant='secondary' onClick={toggleShowColorPicker}>Cancel</Button>
      </div>
    )
  })

  const handleClick = () => {
    colorPickerPackageRef.current.focus();
  }

  const showColorPickerMenuPackage = () => {
    return (
      <div id='colorPickerPackage' onBlur={onBlurHandler} style={{ display: 'inline-block' }} onFocus={onFocusHandler} >
        <Button style={{ marginTop: 10 }} onClick={() => setShowColorPicker(!showColorPicker)}>{showColorPicker ? 'Hide color picker' : 'Add a new color...'}</Button>
        {showColorPicker ?
          <AddColorPicker ref={colorPickerPackageRef} /> : null}
      </div>
    )
  }

  const addColorMenu = () => {
    return (
      <>
        {displayCurrentColors()}
        {showColorPickerMenuPackage()}
      </>
    )
  }

  return (
    <>
      <Button className="ml-2 mr-2" onClick={() => toggleShowAddColorMenu()}>{showAddColorMenu ? 'Hide board colors' : 'Show board colors'}</Button>
      {showAddColorMenu ?
        addColorMenu() :
        null}

    </>
  )

}

export default ColorOptions;