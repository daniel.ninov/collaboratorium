import React, { useState } from 'react';

const DisplaySingleColorOption = ({ color, removeColor, addNewColor, colorId }) => {
  const [showXButton, setShowXButton] = useState(false);

  const handleRemoveColor = () => {
    removeColor(color);
  }

  return (
    <div className='d-flex justify-content-between'>
      <div>
        {color}
      </div>
      <div style={{ backgroundColor: color, width: '40px', height: '20px' }} onMouseEnter={() => setShowXButton(true)} onMouseLeave={() => setShowXButton(false)} className='d-flex justify-content-center'>
          {showXButton ? 
          <button style={{ cursor: 'pointer' }} className='d-flex align-items-center' onClick={() => handleRemoveColor(color)}>x</button> : 
          null}

      </div>
    </div>
  )

}

export default DisplaySingleColorOption;