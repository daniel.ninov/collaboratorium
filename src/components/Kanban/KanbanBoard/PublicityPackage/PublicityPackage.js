import React from 'react';
import { Button } from 'react-bootstrap';

const PublicityPackage = ({ board, setBoard, writeBoardData }) => {

  const togglePublicity = () => {
    const newBoard = {...board};
    newBoard.details.isPublic = !newBoard.details.isPublic;

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  return (
    <div>
      <p>Board state: {board.details.isPublic ? 'Public' : 'Private'}</p>
      <Button onClick={togglePublicity}> Toggle publicity</Button>
    </div>
  );

};

export default PublicityPackage;