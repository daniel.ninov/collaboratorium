import React, { useState, useEffect, useRef } from "react";
import Board, { moveCard, addColumn, moveColumn, addCard } from "@lourenci/react-kanban";
import "@lourenci/react-kanban/dist/styles.css";
import Card from "../Card/Card";
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import { Button, Toast } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import RestrictedBoard from "../RestrictedBoard/RestrictedBoard";
import KanbanBoardInvite from "../KanbanBoardInvite/KanbanBoardInvite";
import ColumnHeader from "../ColumnHeader/ColumnHeader";
import domtoimage from 'dom-to-image';
import ColorOptions from "../ColorOptions/ColorOptions";
import PublicityPackage from "./PublicityPackage/PublicityPackage";
import ChatWindow from "../../Chat/ChatWindow/ChatWindow";
import { withRouter } from "react-router-dom";

const templateBoard = {
  columns: [
    {
      id: 1,
      title: "Column",
      cards: [
        {
          id: 1,
          title: "Card title",
          description: "Card content",
          assignee: null,
        },
      ],
      color: '',
    },
  ],
  details: {},
};

function KanbanBoard({ match, history }) {

  const [controlledBoard, setBoard] = useState(templateBoard);
  const [loading, setLoading] = useState(false);
  const [showTakeMeHome, setShowTakeMeHome] = useState(false);
  const [restricted, setRestricted] = useState(false);
  const [user, userLoading] = useAuthState(firebase.auth());

  const [showColorExistsToast, setShowColorExistsToast] = useState(false);
  const toggleShowColorExistsToast = () => setShowColorExistsToast(!showColorExistsToast);

  const [showZeroColumnsToast, setShowZeroColumnsToast] = useState(false);
  const toggleShowZeroColumnsToast = () => setShowZeroColumnsToast(!showZeroColumnsToast);

  const currentLoggedUser = {
    id: user.uid,
    username: user.providerData[0].displayName,
  };

  const boardRef = useRef(null);

  const boardId = match.params.id;

  const writeBoardData = (board) => {
    firebase.database().ref('boards/' + boardId).set({
      columns: board.columns,
      details: { ...board.details },
    });
  };

  useEffect(() => {

    const checkAccess = (details) => {
      if (details.allowedIds && !userLoading) {
        setRestricted(details.isPublic ? false : !details.allowedIds.includes(user.uid));
      }
    };

    setLoading(true);
    firebase.database().ref('boards/' + boardId).on('value', snapshot => {

      if (snapshot.val() === null) {
        alert(`No data found for these parameters. Are you sure you have the correct address?`);
        setLoading(false);
        setShowTakeMeHome(true);
        return null;
      }

      if (snapshot) {
        checkAccess(snapshot.val().details);

        const fixedBoard = {
          columns: snapshot.val().columns.map(column => {
            return {
              cards: column.cards || [],
              ...column,
            };
          }),
          details: snapshot.val().details,
        };

        setBoard(fixedBoard);
      }

      setLoading(false);
    });

    return () => {
      firebase.database().ref('boards/' + boardId).off('value');
    };
  }, [boardId, user.uid, userLoading])

  function handleCardMove(_card, source, destination) {
    const updatedBoard = moveCard(controlledBoard, source, destination);
    setBoard(updatedBoard);

    writeBoardData(updatedBoard);
  }

  function handleAddColumn() {

    const testColumnToBeAdded = {
      id: controlledBoard.details.currentColumnId + 1,
      title: "New Column",
      cards: []
    };

    const newBoard = addColumn(controlledBoard, testColumnToBeAdded);

    newBoard.details.currentColumnId++;

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleColumnMove(_column, source, destination) {
    const updatedBoard = moveColumn(controlledBoard, source, destination);

    setBoard(updatedBoard);

    writeBoardData(updatedBoard);
  }

  const zeroColumnsToast = () => {
    return (
      <div>
        <Toast show={showZeroColumnsToast}
          onClose={toggleShowZeroColumnsToast}
          delay={3000}
          autohide
          style={{
            position: 'fixed',
            top: '1em',
            right: '1em',
          }}>
          <Toast.Header>
            <strong className="mr-auto">Collaboratorium</strong>
          </Toast.Header>
          <Toast.Body>What's a Kanban board with no columns?</Toast.Body>
        </Toast>
      </div>
    )
  }

  function handleColumnRemove(column) {

    const boardCopy = { ...controlledBoard };
    const newBoard = { ...boardCopy, columns: boardCopy.columns.filter((elem) => elem.id !== column.id) };

    if (newBoard.columns.length === 0) {
      toggleShowZeroColumnsToast();
      const defaultColumn = {
        id: newBoard.details.currentColumnId + 1,
        title: "You can rename me, but please don't remove me :(",
        cards: [
          {
            id: newBoard.details.currentCardId + 1,
            title: "like, whatever",
            description: "you can change this as well",
            assignee: null,
          },
        ],
        color: '',
      };

      newBoard.details.currentCardId++;
      newBoard.details.currentColumnId++;

      newBoard.columns = [defaultColumn];
    }

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleCardDescriptionChange(updatedCard) {

    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === updatedCard.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].description = updatedCard.description;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  };

  function handleDueDateUpdate(updatedCard) {
    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === updatedCard.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].dueDate = updatedCard.dueDate;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleStartDateUpdate(updatedCard) {
    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === updatedCard.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].startDate = updatedCard.startDate;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleColumnColorChange(updatedColumn) {
    const newBoard = {
      ...controlledBoard, columns: controlledBoard.columns.map(column => {
        if (column.id === updatedColumn.id) {
          column.color = updatedColumn.color;
        };

        return column;
      })
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleCardColorChange(updatedCard) {

    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === updatedCard.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].color = updatedCard.color;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleAddCard() {

    const newCard = {
      id: controlledBoard.details.currentCardId + 1,
      title: `New Card`,
      description: "Change me!",
      color: null,
    };

    const newBoard = addCard(controlledBoard, controlledBoard.columns[0], newCard);

    newBoard.details.currentCardId++;

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function handleCardRemove(card) {

    const newBoard = { ...controlledBoard };

    newBoard.columns.forEach(column => column.cards = column.cards.filter((currCard) => currCard.id !== card.id));

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function renderCard(card, cardBag) {
    return (
      <Card
        passedCard={card}
        dragging={cardBag.dragging}
        handleCardDescriptionChange={handleCardDescriptionChange}
        handleCardRemove={handleCardRemove} handleCardRename={handleCardRename}
        colors={controlledBoard.details.colors ? controlledBoard.details.colors : []}
        handleCardColorChange={handleCardColorChange}
        handleDueDateUpdate={handleDueDateUpdate}
        handleStartDateUpdate={handleStartDateUpdate}
        handleAssignCard={handleAssignCard}
      />
    )
  }

  function renderColumnHeader(column, columnBag) {
    return (
      <ColumnHeader
        passedColumn={column}
        allowRenameColumn
        onColumnRename={handleColumnRename}
        allowRemoveColumn
        onColumnRemove={handleColumnRemove}
        colors={controlledBoard.details.colors ? controlledBoard.details.colors : []}
        handleColumnColorChange={handleColumnColorChange}
      />
    )
  }

  function handleCardRename(card) {

    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === card.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].title = card.title;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  };

  const handleColumnRename = (columnToBeChanged, newColumnName) => {

    const boardCopy = { ...controlledBoard };
    boardCopy.columns.forEach(elem => {
      if (elem.id === columnToBeChanged.id) elem.title = newColumnName;
    });

    setBoard(boardCopy);

    writeBoardData(boardCopy);

  };

  const handleAssignCard = (updatedCard) => {
    const newBoard = { ...controlledBoard };

    let cardColumn = undefined;
    let cardIndex = undefined;
    newBoard.columns.forEach((elem, index) => {
      const cardIndexInColumn = elem.cards.findIndex(e => e.id === updatedCard.id);
      if (cardIndexInColumn !== -1) {
        cardColumn = index;
        cardIndex = cardIndexInColumn;
      };
    });

    if (cardColumn !== undefined) {
      newBoard.columns[cardColumn].cards[cardIndex].assignee = updatedCard.assignee;
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  };

  const takeMeHome = () => {
    history.push('/home');
  };

  const handleUploadDataUrl = async () => {
    writeBoardDataUrl(await boardToDataUrl());
  };

  const boardToDataUrl = async () => {
    let dataUrl;

    await domtoimage.toPng(boardRef.current, { bgcolor: 'white' })
      .then(res => {
        dataUrl = res;
      })
      .catch(function (error) {
        console.log('oops, something went wrong!', error);
      });

    return dataUrl;
  }

  function writeBoardDataUrl(dataUrl) {
    firebase.database().ref('exports/' + user.uid).set({
      link: boardId,
      data: dataUrl,
    });
  };

  function removeColor(color) {
    const newBoard = {
      ...controlledBoard
    }
    newBoard.details.colors = newBoard.details.colors.filter(e => {
      if (e === color) return null;
      else return e;
    })

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  function addColor(color) {
    const newBoard = {
      ...controlledBoard
    };

    if (newBoard.details.colors) {
      let colorExistsPointer = false;
      newBoard.details.colors.forEach(elem => {
        if (elem === color) {
          toggleShowColorExistsToast();
          colorExistsPointer = true;
        }
      });

      if (!colorExistsPointer) {
        newBoard.details.colors.push(color);
      };
    } else {
      newBoard.details.colors = [0, color];
    };

    setBoard(newBoard);

    writeBoardData(newBoard);
  }

  const colorExistsToast = () => {
    return (
      <div>
        <Toast show={showColorExistsToast}
          onClose={toggleShowColorExistsToast}
          delay={3000}
          autohide
          style={{
            position: 'fixed',
            top: '1em',
            right: '1em',
          }}>
          <Toast.Header>
            <strong className="mr-auto">Collaboratorium</strong>
          </Toast.Header>
          <Toast.Body>You already have this color. Try a new one.</Toast.Body>
        </Toast>
      </div>
    )
  }


  if (loading) {
    return <h1>LOADING</h1>
  }

  if (showTakeMeHome) {
    return (
      <Button onClick={takeMeHome}>Take me home.</Button>
    );
  };

  if (restricted) {
    return (
      <RestrictedBoard redirect={takeMeHome}></RestrictedBoard>
    );
  }

  return (
    <div>
      <div style={{ maxHeight: '600px', minHeight: '600px', overflow: 'scroll', marginBottom: 10 }}>
        <div id='boardDiv' ref={boardRef}>
          <Board
            renderCard={renderCard}
            renderColumnHeader={renderColumnHeader}
            onCardDragEnd={handleCardMove}
            onColumnDragEnd={handleColumnMove}
            allowColumnDrag>
            {controlledBoard}
          </Board>
        </div>
      </div>

      <div className="card bg-secondary float-left" style={{marginRight: 10, marginBottom: 10, minWidth: 832, maxWidth: 832 }}>
        <div className="card-header" style={{ paddingBottom: 5, paddingTop: 5 }}>Tools</div>
        <div className="card-body">
          <Button className="mr-2" onClick={handleAddColumn}> Add basic column</Button>
          <Button className="mr-2" onClick={handleAddCard}> Add basic card</Button>
          <Button className="mr-2" onClick={() => handleUploadDataUrl()}>Export Kanban</Button>
          <KanbanBoardInvite boardId={boardId} />
          <ColorOptions
            boardColors={controlledBoard.details.colors ? controlledBoard.details.colors : null}
            removeColor={removeColor}
            addColor={addColor}
          />
        </div>
      </div>

      {currentLoggedUser.id === controlledBoard.details.ownerId ?
        <div className="card bg-secondary float-right" style={{ marginBottom: 10 }}>
          <div className="card-header" style={{ paddingBottom: 5, paddingTop: 5 }}>Publicity</div>
          <div className="card-body">
            <PublicityPackage board={controlledBoard} writeBoardData={writeBoardData} setBoard={setBoard} />
          </div>
        </div> : null}

      {colorExistsToast()}
      {zeroColumnsToast()}
      <ChatWindow boardId={boardId} room='boards/' boardTitle={controlledBoard.details.title} />
    </div>
  );
}


export default withRouter(KanbanBoard);
