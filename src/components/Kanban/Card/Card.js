import React, { useState, useEffect } from 'react';
import EditCardModal from './EditCardModal/EditCardModal';


const Card = ({ passedCard, dragging, handleCardRemove, handleCardRename, handleCardDescriptionChange, colors, handleCardColorChange, handleDueDateUpdate, handleStartDateUpdate, handleAssignCard }) => {

  const [cardState, setCardState] = useState({
    id: null,
    title: null,
    color: null,
    description: '',
    dueDate: '',
  });

  const [reloadMarker, setReloadMarker] = useState(true);

  useEffect(() => {
    setCardState({
      id: passedCard.id,
      title: passedCard.title,
      color: passedCard.color ? passedCard.color : null,
      description: passedCard.description ? passedCard.description : null,
      dueDate: passedCard.dueDate ? passedCard.dueDate : null,
      startDate: passedCard.startDate ? passedCard.startDate : null,
      assignee: passedCard.assignee ? passedCard.assignee : null,
    });
  }, [passedCard, reloadMarker])

  const [showRename, setShowRename] = useState(false);

  const renameCard = () => {
    return (
      <span>
        <div style={{ display: "flex" }}>
          <span style={{
            display: 'inline-flex',
            alignItems: 'center'
          }}>
            <input type='text' style={{ fontSize: 16, maxWidth: 130, minWidth: 130, marginLeft: 20, marginRight: 12 }} value={cardState.title} onChange={e => setCardState({ ...cardState, title: e.target.value })} />
          </span>
          <div>
            <span style={{ cursor: 'pointer', fontSize: 35 }} onClick={() => { handleCardRename(cardState); setShowRename(false); }}>
              √
						</span>
            <span style={{ cursor: 'pointer', fontSize: 40 }} onClick={() => { setReloadMarker(!reloadMarker); setShowRename(false) }}>
              ×
						 </span>
          </div>
        </div>
      </span>
    )
  };

  return (
    <div className={`react-kanban-card ${dragging ? 'react-kanban-card--dragging' : ''}`}>

      {showRename ?
        renameCard() :
        <span>
          <div style={{ display: "flex" }}>
            {cardState.color ?
              <div className='react-kanban-card-color-box' style={{ backgroundColor: cardState.color }} /> :
              null
            }
            <span style={{
              display: 'inline-flex',
              alignItems: 'center',
              minWidth: 120,
              maxWidth: 120,
              fontSize: 17,
              marginLeft: 10
            }} onClick={() => setShowRename(true)}>{cardState.title}</span>
            <EditCardModal
              cardState={cardState}
              setCardState={setCardState}
              handleCardDescriptionChange={handleCardDescriptionChange}
              colors={colors}
              handleDueDateUpdate={handleDueDateUpdate}
              handleStartDateUpdate={handleStartDateUpdate}
              handleCardColorChange={handleCardColorChange}
              handleAssignCard={handleAssignCard}
            />
            <div style={{ cursor: 'pointer', fontSize: 40, float: 'right' }} onClick={() => handleCardRemove(passedCard)}>
              ×
            </div>
          </div>
        </span>
      }
    </div>
  )
};


export default Card;