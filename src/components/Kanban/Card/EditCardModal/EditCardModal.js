import React, { useState, useEffect } from 'react';
import { Modal, Button, Dropdown } from 'react-bootstrap';
import moment from 'moment';
import AssignCardModal from '../../AssignCardModal/AssignCardModal';
import * as firebase from 'firebase/app';
import 'firebase/database';

const EditCardModal = ({ cardState, handleCardDescriptionChange, colors, handleCardColorChange, handleDueDateUpdate, handleStartDateUpdate, handleAssignCard }) => {

  const [show, setShow] = useState(false);
  const [editCardState, setEditCardState] = useState({
    description: '',
    title: '',
    id: null,
    color: '',
    dueDate: '',
    startDate: '',
    assignee: '',
  });
  const [assigneeDisplayName, setAssigneeDisplayName] = useState('');
  const [showEditDescription, setShowEditDescription] = useState(false);
  const [showDueDateSetter, setShowDueDateSetter] = useState(false);
  const [showStartDateSetter, setShowStartDateSetter] = useState(false);
  const [dueDateValue, setDueDateValue] = useState(moment().format('YYYY-MM-DD'));
  const [startDateValue, setStartDateValue] = useState(moment().format('YYYY-MM-DD'));

  useEffect(() => {
    setEditCardState({
      description: cardState.description,
      color: cardState.color,
      id: cardState.id,
      title: cardState.title,
      dueDate: cardState.dueDate ? cardState.dueDate : null,
      startDate: cardState.startDate ? cardState.startDate : null,
      assignee: cardState.assignee ? cardState.assignee : null,
    });

    if (cardState.assignee) {
      async function placeholder() {
        await firebase.database().ref(`users/${cardState.assignee}`).once('value')
          .then(user => {
            if (!user.val()) throw new Error('No such user found.')
            setAssigneeDisplayName(user.val().displayName);
          })
          .catch(e => console.log(e.message));

      }

      placeholder();
    }

  }, [cardState]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleSubmitDescription = () => {
    handleCardDescriptionChange(editCardState);
    setShowEditDescription(false);
  };

  const handleCancelDescription = () => {
    setEditCardState({ ...editCardState, description: cardState.description });
    setShowEditDescription(false);
  };

  const editDescription = () => {
    return (
      <div>
        <textarea className="form-control" rows='4' cols='50' value={editCardState.description} onChange={e => setEditCardState({ ...editCardState, description: e.target.value })} />
        <Button variant='secondary' onClick={handleCancelDescription}>Cancel</Button>
        <Button variant='primary' onClick={handleSubmitDescription} disabled={(editCardState.description.length > 1024)}>Submit</Button>
      </div>
    )
  }

  const handleSubmitDueDate = () => {
    if (moment(dueDateValue))

      handleDueDateUpdate({ ...editCardState, dueDate: dueDateValue });
    setShowDueDateSetter(false);
  }

  const handleSubmitStartDate = () => {
    if (moment(startDateValue))

      handleStartDateUpdate({ ...editCardState, startDate: startDateValue });
    setShowStartDateSetter(false);
  }

  const dueDateSetter = () => {
    return (
      <div>
        <input type='date' min={editCardState.startDate ? editCardState.startDate : moment().format('YYYY-MM-DD')} value={dueDateValue} onChange={e => setDueDateValue(e.target.value)} />
        <Button variant='primary' onClick={() => handleSubmitDueDate()}>UpDate!</Button>
        <Button variant='secondary' onClick={() => setShowDueDateSetter(false)}>Cancel</Button>
      </div>
    )
  };

  const startDateSetter = () => {
    return (
      <div>
        <input type='date' min={moment().format('YYYY-MM-DD')} max={editCardState.dueDate ? editCardState.dueDate : null} value={startDateValue} onChange={e => setStartDateValue(e.target.value)} />
        <Button variant='primary' onClick={() => handleSubmitStartDate()}>UpDate!</Button>
        <Button variant='secondary' onClick={() => setShowStartDateSetter(false)}>Cancel</Button>
      </div>
    )
  };


  return (
    <div>
      <span style={{ cursor: 'pointer', fontSize: 40 }} onClick={handleShow}>⌘</span>

      <Modal style={{}} show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit {cardState.title}</Modal.Title>
        </Modal.Header>

        <Modal.Body>

          <div style={{ alignItems: 'center', display: 'flex' }}>
            Current color: {cardState.color ?
              <div className='react-kanban-card-color-box' style={{ backgroundColor: cardState.color, width: '10%', height: '20px' }} /> :
              <div>No color has been added</div>
            }
            <div style={{ marginLeft: 'auto' }}>
              <Dropdown>
                <Dropdown.Toggle id='dropdown-basic' style={{
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingLeft: 5,
                  paddingRight: 5,
                  marginLeft: 5,
                }}>Change color</Dropdown.Toggle>

                <Dropdown.Menu>
                  {colors.map(color => {
                    if (color === 0) return null;
                    return <Dropdown.Item key={color} id={color} as='button' onClick={e => handleCardColorChange({ ...cardState, color: e.target.id })} style={{ backgroundColor: color, margin: '5px', height: '20px', width: '20px' }} />
                  })}

                  <Dropdown.Item id='remove-color' as='button' onClick={e => handleCardColorChange({ ...cardState, color: null })}>Remove color</Dropdown.Item>
                </Dropdown.Menu>

              </Dropdown>
            </div>
          </div>

          <div>
            {showEditDescription ?
              editDescription() :
              <div className="card-description" onClick={() => setShowEditDescription(true)}>
                {cardState.description}
              </div>}
          </div>

          <div styles={{ marginTop: 10 }}>
            {cardState.dueDate ?
              'Due date: ' + moment(cardState.dueDate).format('MMM Do YY') :
              'No due date set.'}
          </div>

          <div>
            {cardState.startDate ?
              'Start date: ' + moment(cardState.startDate).format('MMM Do YY') :
              'No start date set.'}
          </div>

          {showDueDateSetter ?
            dueDateSetter() :
            <Button style={{
              paddingTop: 0,
              paddingBottom: 0,
              paddingLeft: 5,
              paddingRight: 5,
              marginTop: 10,
            }} onClick={() => setShowDueDateSetter(true)}>Set Due Date...</Button>}

          {showStartDateSetter ?
            startDateSetter() :
            <Button style={{
              paddingTop: 0,
              paddingBottom: 0,
              paddingLeft: 5,
              paddingRight: 5,
              marginLeft: 5,
              marginTop: 10,
            }} onClick={() => setShowStartDateSetter(true)}>Set Start Date...</Button>}

          <div style={{ marginTop: 10 }}>
            {cardState.assignee ?
              'Assignee: ' + assigneeDisplayName
              : 'No one has been assigned to this card.'}
          </div>

          <AssignCardModal cardState={cardState} handleAssignCard={handleAssignCard} />

        </Modal.Body>

        <Modal.Footer>
          <Button variant="outline-primary" onClick={handleClose}>Close</Button>
        </Modal.Footer>

      </Modal>
    </div>
  )

};

export default EditCardModal;