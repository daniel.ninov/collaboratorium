import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const RestrictedBoard = ({ redirect }) => {
  const [show, setShow] = useState(true);

  const clearRestrict = () => {
    setShow(false);
    redirect();
  };

  const renderModal = () => {
    return (
      <>
        <Modal show={show} onHide={clearRestrict}>
          <Modal.Header>
            <Modal.Title>Restricted Board</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4>You do not have access to this board.</h4>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={clearRestrict}>
              Alright
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  };

  return renderModal();
};

export default RestrictedBoard;