import React, { useState, useEffect } from 'react';
import KanbanBoardLink from './../KanbanBoardLink/KanbanBoardLink';
import * as firebase from 'firebase/app';
import 'firebase/database';
import CreateBoardModule from '../CreateBoardModule/CreateBoardModule';

const PublicBoardsPage = () => {

  const [publicBoards, setPublicBoards] = useState([]);
  const database = firebase.database();

  useEffect(() => {

    const countersRef = database.ref('boardsCounters');
    
    const boardsRef = database.ref('boards');
    countersRef.on('value', function(snapshot) {
      boardsRef.once('value', function (snapshot) {
        const allBoards = Object.values(snapshot.val());
        const onlyPublicBoards = allBoards.filter(elem => elem.details?.isPublic);
  
        setPublicBoards(onlyPublicBoards);
      });

    })

    return () => {
      countersRef.off();
      boardsRef.off();
    };
  }, [database]);

  return (
    <div>
      {publicBoards.map(elem => <KanbanBoardLink key={elem.details.id} board={elem} />)}
      <CreateBoardModule publicity={true} />
    </div>
  )
};

export default PublicBoardsPage;