import React, { useState } from 'react';
import { EMAIL_REGEX } from '../../../common/constraints';
import Modal from 'react-bootstrap/Modal';
import * as firebase from 'firebase/app';
import 'firebase/auth';

const ResetPassword = ({ history }) => {
  const [user, setUser] = useState({
    email: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const [sent, setSent] = useState(false);
  const handleClose = () => {
    setSent(false);
    history.push('/login');
  }

  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && validatorFn(value), true),
    }
  });

  const userValidators = {
    email: [
      value => value && value.length >= 3,
      value => value && EMAIL_REGEX.test(value)
    ],
  };

  const getClassNames = (prop) => {
    let classes = '';

    if (!user[prop].valid) {
      classes += ' invalid ';
    }

    return classes;
  };

  const validateForm = () => !Object
    .keys(user)
    .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

  const reset = (e) => {
    e.preventDefault();

    firebase.auth().sendPasswordResetEmail(user.email.value).then(() => {
      setSent(true);
    }).catch(err => setSent(true));
  }

  const renderForm = () => {
    return (
      <>
        <Modal show={sent} onHide={handleClose}>
          <Modal.Header>
            <Modal.Title>Reset Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row col-sm-10">A password reset email has been send to your email address.</div>
          </Modal.Body>
          <Modal.Footer>
            <button className="btn btn-primary" onClick={handleClose}>Alright</button>
          </Modal.Footer>
        </Modal>

        <div className="card">
          <div className="card-header">
            <h4 className="font-weight-normal">Reset Password</h4>
          </div>
          <div className="card-body">
            <form>
              <div className="form-group row">
                <label htmlFor="inputEmail" className="col-sm-2 col-form-label">
                  Email
              </label>
                <div className="col-sm-10">
                  <input type="email" className={getClassNames('email')}
                    id="inputEmail" value={user.email.value}
                    onChange={e => updateUser('email', e.target.value)} />
                </div>
              </div>
              <div className="form-group row float-right">
                <div className="col-auto">
                  <button type="submit"
                    className="btn btn-primary"
                    onClick={e => reset(e)}
                    disabled={validateForm()}>
                    Reset
                </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }

  return renderForm();
};

export default ResetPassword;