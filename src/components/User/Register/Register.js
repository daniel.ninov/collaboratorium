import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import AuthContext from '../../../providers/AuthContext';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import AppError from '../../Base/Error/Error';
import { EMAIL_REGEX } from '../../../common/constraints';

const Register = ({ history }) => {
  const [user, setUser] = useState({
    email: {
      value: '',
      touched: false,
      valid: true,
    },
    displayName: {
      value: '',
      touched: false,
      valid: true,
    },
    password: {
      value: '',
      touched: false,
      valid: true,
    },
    password2: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const [error, setError] = useState(null);
  const { currentUser, setCurrentUser } = useContext(AuthContext);

  useEffect(() => {
    if (currentUser) {
      history.push('/home');
    }
  }, [history, currentUser]);

  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && validatorFn(value), true),
    }
  });

  const userValidators = {
    email: [
      value => value && value.length >= 3,
      value => value && EMAIL_REGEX.test(value)
    ],
    password: [
      value => value && value.length >= 6,
    ],
    password2: [
      value => value === user.password.value,
    ],
    displayName: [
      value => value && value.length >= 5,
      value => value && value.length <= 60,
    ],
  };

  const getClassNames = (prop) => {
    let classes = '';

    if (!user[prop].valid) {
      classes += ' invalid ';
    }

    return classes;
  };

  const validateForm = () => !Object
    .keys(user)
    .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

  const saveUserToDB = (user, displayName) => {
    firebase.database().ref('users/' + user.uid).set({
      uid: user.uid,
      displayName,
      email: user.email,
    })
  };
  
  const register = (e) => {
    e.preventDefault();

    firebase.auth().createUserWithEmailAndPassword(user.email.value, user.password.value)
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }

        result.user.updateProfile({
          displayName: user.displayName.value,
        });

        saveUserToDB(result.user, user.displayName.value);
        
      }).then(() => {
        firebase.auth().signOut().catch(error => console.log(error.message));
        
        setCurrentUser({
          user: null,
        });

        history.push('/login');
      }).catch(error => setError(error.message));
  };

  if (error) {
    return (
      <AppError message={error} setError={() => setError()} />
    );
  }

  return (
    <div className="card">
      <div className="card-header">
        <h4 className="font-weight-normal">Register</h4>
      </div>
      <div className="card-body">
        <form>
          <div className="form-group row">
            <label htmlFor="inputEmail" className="col-sm-2 col-form-label">
              Email
                            </label>
            <div className="col-sm-10">
              <input type="email" className={getClassNames('email')}
                id="inputEmail" value={user.email.value}
                onChange={e => updateUser('email', e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="inputDisplayName" className="col-sm-2 col-form-label">
              Display Name
                            </label>
            <div className="col-sm-10">
              <input type="text" className={getClassNames('displayName')}
                id="inputDisplayName" value={user.displayName.value}
                onChange={e => updateUser('displayName', e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="inputPassword" className="col-sm-2 col-form-label">
              Password
                            </label>
            <div className="col-sm-10">
              <input type="password" className={getClassNames('password')}
                id="inputPassword" value={user.password.value}
                onChange={e => updateUser('password', e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="inputConfirmPassword" className="col-sm-2 col-form-label">
              Confirm Password
                            </label>
            <div className="col-sm-10">
              <input type="password" className={getClassNames('password2')}
                id="inputConfirmPassword" value={user.password2.value}
                onChange={e => updateUser('password2', e.target.value)} />
            </div>
          </div>
          <div className="form-group row float-right">
            <div className="col-auto">
              <button type="submit" className="btn btn-primary" onClick={e => register(e)} disabled={validateForm()}>Register</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

Register.propTypes = {
  history: PropTypes.object.isRequired,
}

export default Register;