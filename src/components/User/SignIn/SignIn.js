import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import AuthContext from '../../../providers/AuthContext';
import { EMAIL_REGEX } from '../../../common/constraints';
import firebase from 'firebase/app';
import 'firebase/auth';
import AppError from '../../Base/Error/Error';
import { withRouter } from 'react-router-dom';

const SignIn = ({ history }) => {
  const [user, setUser] = useState({
    email: {
      value: '',
      touched: false,
      valid: true,
    },
    password: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const [error, setError] = useState(null);
  const { currentUser } = useContext(AuthContext);

  useEffect(() => {
    if (currentUser) {
      history.push('/home');
    }
  }, [history, currentUser]);

  const updateUser = (prop, value) => setUser({
    ...user,
    [prop]: {
      value,
      touched: true,
      valid: userValidators[prop].reduce((isValid, validatorFn) => isValid && validatorFn(value), true),
    }
  });

  const userValidators = {
    email: [
      value => value && value.length >= 3,
      value => value && EMAIL_REGEX.test(value)
    ],
    password: [
      value => value && value.length >= 6,
    ],
  };

  const getClassNames = (prop) => {
    let classes = '';

    if (!user[prop].valid) {
      classes += ' invalid ';
    }

    return classes;
  };

  const validateForm = () => !Object
    .keys(user)
    .reduce((isValid, prop) => isValid && user[prop].valid && user[prop].touched, true);

  const signIn = (e) => {
    e.preventDefault();

    firebase.auth().signInWithEmailAndPassword(user.email.value, user.password.value)
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }

        history.push('/home');
      }).catch(error => setError(error.message));
  };

  const resetPassword = (e) => {
    e.preventDefault();

    history.push('/reset');
  }

  if (error) {
    return (
      <AppError message={error} setError={() => setError()} />
    );
  }

  return (
    <div className="card">
      <div className="card-header">
        <h4 className="font-weight-normal">Sign In</h4>
      </div>
      <div className="card-body">
        <form>
          <div className="form-group row">
            <label htmlFor="inputEmail" className="col-sm-2 col-form-label">
              Email
                            </label>
            <div className="col-sm-10">
              <input type="email" className={getClassNames('email')}
                id="inputEmail" value={user.email.value}
                onChange={e => updateUser('email', e.target.value)} />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="inputPassword" className="col-sm-2 col-form-label">
              Password
                            </label>
            <div className="col-sm-10">
              <input type="password" className={getClassNames('password')}
                id="inputPassword" value={user.password.value}
                onChange={e => updateUser('password', e.target.value)} />
            </div>
          </div>
          <div className="form-group row float-right">
            <div className="col-auto">
              <button type="submit" className="btn btn-primary" onClick={e => signIn(e)} disabled={validateForm()}>Sign In</button>
            </div>
          </div>
        </form>
        <div className="col-auto mr-1 float-right">
          <button className="btn btn-primary" onClick={e => resetPassword(e)}>Reset Password</button>
        </div>
      </div>
    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
}

export default withRouter(SignIn);