import React from 'react';
import { withRouter } from 'react-router-dom';

const SingleCard = ({ boardId, boardTitle, card, history }) => {


  const openBoard = () => {
    history.push(`boards/${boardId}`);
  }

  return (
    <div className='cardA hoverableA flex-centeredA' onClick={openBoard} >
      <div className='card-body'>
        <h4>{card.title}</h4>
        <h6>Description: {card.description}</h6>
        {card.dueDate ? <h6>Due date: {card.dueDate}</h6> : null}
        {card.startDate ? <h6>Start date: {card.startDate}</h6> : null}
        <h6>From board {boardTitle}</h6>
        <h6>Click to go to Kanban</h6>
      </div>
    </div>
  )
};


export default withRouter(SingleCard);