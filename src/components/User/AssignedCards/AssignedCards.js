import React, { useEffect, useState } from 'react';
import * as firebase from 'firebase/app';
import 'firebase/database';
import SingleCard from './SingleCard/SingleCard';

const AssignedCards = ({ userId }) => {

  const [packages, setPackages] = useState([{}]);
  const boardsRef = firebase.database().ref('boards/');
  let cardKey = 0;

  useEffect(() => {
    let allBoards = [];

    boardsRef.once('value')
      .then(res => {
        allBoards = res.val();
      })
      .then(res => {

        const allBoardsArr = Object.values(allBoards);
        const deepArrayOfPackages = allBoardsArr.map(board => {
          const boardCardsPackage = {
            boardId: board.details.id,
            boardTitle: board.details.title,
            columns: board.columns.map(column => {
              return column.cards?.filter(card => card.assignee === userId);
            }),
          }

          boardCardsPackage.columns = boardCardsPackage.columns.filter(column => column.length !== 0);

          if (boardCardsPackage.columns?.length !== 0) return boardCardsPackage;
          else return null;
        });

        const filteredArrayOfPackages = deepArrayOfPackages.filter(elem => elem !== null);

        setPackages(filteredArrayOfPackages);
      })
      .catch(e => console.log(e.message));
  }, [boardsRef]);

  if (packages.length === 0) return null;


  return (
    <div>
      <h3>Assigned cards</h3>
      <hr />
      <div style={{ display: "flex", flexDirection: 'column', justifyContent: 'center', alignItems:'stretch',  }}>
        {packages.map(singlePackage => {
          return singlePackage.columns?.map(column => column.map(card =>
            <SingleCard key={cardKey++} card={card} boardId={singlePackage.boardId} boardTitle={singlePackage.boardTitle} />))
        })}

      </div>
    </div>
  )
};

export default AssignedCards;
