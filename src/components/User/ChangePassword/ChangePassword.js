import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Alert from 'react-bootstrap/Alert';
import * as firebase from 'firebase/app';
import 'firebase/auth';

const ChangePassword = () => {
  const [show, setShow] = useState(false);
  const [invalid, setInvalid] = useState(false);
  const [valid, setValid] = useState(false);
  const [userObj, setUserObj] = useState({
    currPassword: {
      value: '',
      touched: false,
      valid: true,
    },
    newPassword: {
      value: '',
      touched: false,
      valid: true,
    },
    newPassword2: {
      value: '',
      touched: false,
      valid: true,
    },
  });

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleSubmit = (e) => {
    e.preventDefault();

    changePassword();
    setShow(false);
  };

  const userObjValidators = {
    currPassword: [
      value => value && value.length >= 6,
    ],
    newPassword: [
      value => value && value.length >= 6,
    ],
    newPassword2: [
      value => value === userObj.newPassword.value,
    ],
  };

  const updateUserObj = (prop, value) => setUserObj({
    ...userObj,
    [prop]: {
      value,
      touched: true,
      valid: userObjValidators[prop].reduce((isValid, validatorFn) => isValid && validatorFn(value), true),
    }
  });

  const getClassNames = (prop) => {
    let classes = '';

    if (!userObj[prop].valid) {
      classes += ' invalid';
    }

    return classes;
  };

  const validateForm = () => !Object
    .keys(userObj)
    .reduce((isValid, prop) => isValid && userObj[prop].valid && userObj[prop].touched, true);

  const changePassword = () => {
    const user = firebase.auth().currentUser;
    const credential = firebase.auth.EmailAuthProvider.credential(user.email, userObj.currPassword.value);

    user.reauthenticateWithCredential(credential).then(() => {
      user.updatePassword(userObj.newPassword.value)
        .then(() => setValid(true))
        .catch(err => console.log(err.message));
    }).catch(() => setInvalid(true));
  };

  const renderModal = () => {
    return (
      <>
        <div style={{display: 'flex', justifyContent: 'flex-end'}} >
          <Button onClick={handleShow}>
            Change Password
        </Button>
        </div>

        <Alert variant="success"
          show={valid}
          onMouseLeave={() => setValid(false)}>
          Password changed!
        </Alert>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header>
            <Modal.Title>Change Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <div className="form-group row">
                <label htmlFor="currentPassword" className="col-sm-2 col-form-label">
                  Current Password
                </label>
                <div className="col-sm-8">
                  <input type="password" id="currentPassword"
                    className={getClassNames('currPassword')}
                    onChange={e => updateUserObj('currPassword', e.target.value)}
                    value={userObj.currPassword.value} />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="newPassword" className="col-sm-2 col-form-label">
                  New Password
                </label>
                <div className="col-sm-8">
                  <input type="password" id="newPassword"
                    className={getClassNames('newPassword')}
                    onChange={e => updateUserObj('newPassword', e.target.value)}
                    value={userObj.newPassword.value} />
                </div>
              </div>
              <div className="form-group row">
                <label htmlFor="confirmPassword" className="col-sm-2 col-form-label">
                  Confirm Password
                </label>
                <div className="col-sm-8">
                  <input type="password" id="confirmPassword"
                    className={getClassNames('newPassword2')}
                    onChange={e => updateUserObj('newPassword2', e.target.value)}
                    value={userObj.newPassword2.value} />
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <div className="col-auto float-right">
              <Button disabled={validateForm()} onClick={e => handleSubmit(e)}>Submit</Button>
            </div>
          </Modal.Footer>
        </Modal>

        <Modal show={invalid} onHide={() => setInvalid(false)}>
          <Modal.Header>
            <Modal.Title>Invalid Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row col-sm-10">Your current password is invalid.</div>
          </Modal.Body>
          <Modal.Footer>
            <button className="btn btn-primary" onClick={() => setInvalid(false)}>Alright</button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }

  return renderModal();
};

export default ChangePassword;