import React, { useContext } from 'react';
import AuthContext from '../../../providers/AuthContext';
import AssignedCards from '../AssignedCards/AssignedCards';
import ChangePassword from '../ChangePassword/ChangePassword';
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import { useAuthState } from 'react-firebase-hooks/auth';

const Profile = () => {
  const { currentUser } = useContext(AuthContext);


  const [authUser] = useAuthState(firebase.auth());
  const user = { uid: authUser.uid };


  return (
    <>

      <h3>Profile</h3>
      <hr />
      <table>
        <tbody>
          <tr className='table-rowA'>
            <td>
              <h6>Email:</h6>
            </td>
            <td>{currentUser.providerData[0].email}</td>
          </tr>
          <tr className='table-rowA'>
            <td>
              <h6>Display Name:</h6>
            </td>
            <td>{currentUser.providerData[0].displayName}</td>
          </tr>
        </tbody>
      </table>
      <ChangePassword />

      <AssignedCards userId={user.uid} />


    </>
  );
};

export default Profile;