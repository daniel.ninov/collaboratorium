import React from 'react';
import { getCursorHandlerInstance } from '../../Collaboration/Cursors/CursorHandler';
import Cursors from '../../Collaboration/Cursors/Cursors';
import Whiteboard from '../Whiteboard/Whiteboard';

const WhiteboardCursors = (props) => {
    const roomId = props.match.params.id;

    return (
    <>
    <Cursors roomRef={roomId}
    instance={getCursorHandlerInstance && getCursorHandlerInstance(roomId)} />
    <Whiteboard {...props}/>
    </>);
}

export default WhiteboardCursors;