import React, { useState, useEffect } from 'react';
import * as firebase from 'firebase/app';
import { useAuthState } from 'react-firebase-hooks/auth';
import 'firebase/auth';
import WhiteboardLink from '../WhiteboardLink/WhiteboardLink';
import CreateWhiteboard from '../CreateWhiteboard/CreateWhiteboard';

const PersonalWhiteboards = () => {

  const [personalBoards, setPersonalBoards] = useState([]);
  const database = firebase.database();
  const [user] = useAuthState(firebase.auth());

  const currentLoggedUser = {
    id: user.uid,
    username: user.providerData[0].displayName,
  };

  let changedLoggedUserId = currentLoggedUser.id;

  useEffect(() => {

    const countersRef = database.ref('whiteboardsCounters');
    
    const boardsRef = database.ref('drawings');
    countersRef.on('value', function(snapshot) {
      boardsRef.once('value', function (snapshot) {
        const allBoards = Object.values(snapshot.val());
        const onlyPersonalBoards = allBoards.filter(elem => !elem.details?.isPublic && 
            (elem.details.ownerId === changedLoggedUserId || elem.details.allowedIds.includes(changedLoggedUserId)));

        setPersonalBoards(onlyPersonalBoards);
      });

    })

    return () => {
      countersRef.off();
      boardsRef.off();
    };
  }, [database, changedLoggedUserId]);

  return (
    <div>
      {personalBoards.map(elem => <WhiteboardLink key={elem.details.id} whiteboard={elem.details} />)}
      <CreateWhiteboard publicity={false} />
    </div>
  )
};

export default PersonalWhiteboards;
