import React from 'react';
import { SketchField, Tools } from 'react-sketch';
import mousetrap from 'mousetrap';
import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import RestrictedWhiteboard from '../RestrictedWhiteboard/RestrictedWhiteboard';
import { ChromePicker } from 'react-color';
import ChatWindow from '../../Chat/ChatWindow/ChatWindow';

class Whiteboard extends React.Component {
  currentWbId;
  currentUserId;
  curretnWbTitle;

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      text: '',
      lineWidth: 10,
    };

    this.currentWbId = props.match.params.id;
  }

  _selectTool = event => {
    this.setState({
      tool: event.target.value,
      enableRemoveSelected: event.target.value === Tools.Select,
      enableCopyPaste: event.target.value === Tools.Select,
    });

    this._save();
  };

  _keybindTool = tool => {
    this.setState({
      tool,
      enableRemoveSelected: tool === Tools.Select,
      enableCopyPaste: tool === Tools.Select,
    });

    this._save();
  };

  _incWidth = () => {
    if (this.state.lineWidth < 100) {
      this.setState({
        lineWidth: this.state.lineWidth + 10,
      });

      this._save();
    }
  }

  _decWidth = () => {
    if (this.state.lineWidth > 10) {
      this.setState({
        lineWidth: this.state.lineWidth - 10,
      });

      this._save();
    }
  }

  _clear = () => {
    this._sketch.clear();
    this._sketch.setBackgroundFromDataUrl('');
    this.setState({
      controlledValue: null,
      backgroundColor: 'transparent',
      fillWithBackgroundColor: false,
      canUndo: this._sketch.canUndo(),
      canRedo: this._sketch.canRedo(),
      kanbanLink: null,
    });

    this._save();
  };

  _undo = () => {
    this._sketch.undo();
    this.setState({
      canUndo: this._sketch.canUndo(),
      canRedo: this._sketch.canRedo(),
    });

    this._save();
  };

  _redo = () => {
    this._sketch.redo();
    this.setState({
      canUndo: this._sketch.canUndo(),
      canRedo: this._sketch.canRedo(),
    });

    this._save();
  };

  _download = () => {
    console.log(this._sketch.toDataURL(), 'toDataURL.txt');
    console.log(JSON.stringify(this._sketch.toJSON()), 'toDataJSON.txt');
  };

  _onSketchChange = () => {
    let prev = this.state.canUndo;
    let now = this._sketch.canUndo();
    if (prev !== now) {
      this.setState({ canUndo: now });
    }
  };

  _save = () => {
    let drawings = {
      objects: this._sketch.toJSON().objects,
      background: "",
    };

    firebase.database().ref('drawings/' + this.currentWbId + '/board').set(this._fixSave({
      ...this.state,
      drawings
    }));

    this.setState({ drawings });
  };

  _fixSave = (drawings) => {
    return JSON.parse(JSON.stringify(drawings).replace(/null/g, 0));
  };

  _removeSelected = () => {
    this._sketch.removeSelected();

    this._save();
  };

  _handleChangeColor = (color, event) => {
    this.setState({ lineColor: color.hex });

    this._save();
  };

  _addText = () => {
    this._sketch.addText(this.state.text);
    this._save();
  };

  _importKanban = () => {
    firebase.database().ref('exports/' + this.currentUserId).once('value').then(snapshot => {
      if (!!snapshot.val()) {
        this._sketch.addImg(snapshot.val().data);
        this.setState({ kanbanLink: snapshot.val().link });
      }
    }).catch(err => console.log(err.message));
  };

  componentDidMount() {
    this._checkUser();

    firebase.database().ref('drawings/' + this.currentWbId + '/board').on('value', (snapshot) => {
      this.setState(snapshot.val());
    });

    mousetrap.bind('ctrl+z', () => {
      if (this.state.canUndo) {
        this._undo();
      }
    });

    mousetrap.bind('ctrl+y', () => {
      if (this.state.canRedo) {
        this._redo();
      }
    });

    mousetrap.bind('alt+1', () => this._keybindTool(Tools.Select));
    mousetrap.bind('alt+2', () => this._keybindTool(Tools.Pan));
    mousetrap.bind('alt+3', () => this._keybindTool(Tools.Pencil));
    mousetrap.bind('alt+4', () => this._keybindTool(Tools.Line));
    mousetrap.bind('alt+5', () => this._keybindTool(Tools.Rectangle));
    mousetrap.bind('alt+6', () => this._keybindTool(Tools.Circle));

    mousetrap.bind('alt+w', () => this._sketch.zoom(1.25));
    mousetrap.bind('alt+s', () => this._sketch.zoom(0.8));

    mousetrap.bind('backspace', () => {
      if (this.state.enableRemoveSelected) {
        this._removeSelected();
      }
    });

    mousetrap.bind('alt+c', () => {
      if (this.state.enableCopyPaste) {
        this._sketch.copy();
      }
    });
    mousetrap.bind('alt+v', () => {
      if (this.state.enableCopyPaste) {
        this._sketch.paste();
      }
    });

    mousetrap.bind('up', () => this._incWidth());
    mousetrap.bind('down', () => this._decWidth());
  };

  componentWillUnmount() {

    mousetrap.unbind('ctrl+z');
    mousetrap.unbind('ctrl+y');

    mousetrap.unbind('alt+1');
    mousetrap.unbind('alt+2');
    mousetrap.unbind('alt+3');
    mousetrap.unbind('alt+4');
    mousetrap.unbind('alt+5');
    mousetrap.unbind('alt+6');

    mousetrap.unbind('alt+w');
    mousetrap.unbind('alt+s');

    mousetrap.unbind('backspace');

    mousetrap.unbind('alt+c');
    mousetrap.unbind('alt+v');

    mousetrap.unbind('up');
    mousetrap.unbind('down');

    firebase.database().ref('drawings/' + this.currentWbId + '/board').off();
  };

  _checkUser = async () => {
    await firebase.database().ref('drawings/' + this.currentWbId + '/details').once('value').then(snapshot => {
      this.curretnWbTitle = snapshot.val().title;
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this.currentUserId = user.uid;
          if (snapshot.val().isPublic ? false : !snapshot.val().allowedIds.includes(user.uid)) {
            this.setState({ restricted: true });
          }
        }
      });
    });
  }

  render() {
    if (this.state.restricted) {
      return <RestrictedWhiteboard redirect={() => this.props.history.push('/drawings')} />
    }

    return (<div>
      <div className="card border-light mb-3">
        <div className="card-body pt-0 pb-0 pl-0 pr-0">
          <button className="btn btn-primary mr-2" onClick={this._clear}>Clear</button>
          <button className="btn btn-primary mr-2" disabled={!this.state.canUndo} onClick={this._undo}>Undo</button>
          <button className="btn btn-primary mr-2" disabled={!this.state.canRedo} onClick={this._redo}>Redo</button>
          <button className="btn btn-primary mr-2" onClick={this._save}>Save</button>
          <button className="btn btn-primary mr-2" onClick={(e) => this._sketch.zoom(1.25)}>Zoom In</button>
          <button className="btn btn-primary mr-2" onClick={(e) => this._sketch.zoom(0.8)}>Zoom Out</button>
          <button className="btn btn-primary mr-2" disabled={!this.state.enableCopyPaste} onClick={(e) => this._sketch.copy()}>Copy</button>
          <button className="btn btn-primary mr-2" disabled={!this.state.enableCopyPaste} onClick={(e) => this._sketch.paste()}>Paste</button>
          <button className="btn btn-primary mr-2" disabled={!this.state.enableRemoveSelected} onClick={this._removeSelected}>Erase</button>
        </div>
      </div>

      <SketchField
        lineColor={this.state.lineColor}
        lineWidth={this.state.lineWidth}
        fillColor={
          this.state.fillWithColor
            ? this.state.fillColor
            : 'transparent'
        }
        backgroundColor={
          this.state.fillWithBackgroundColor
            ? this.state.backgroundColor
            : 'transparent'
        }
        width={
          this.state.controlledSize ? this.state.sketchWidth : null
        }
        height={
          this.state.controlledSize ? this.state.sketchHeight : null
        }
        ref={c => (this._sketch = c)}
        defaultValue={this.state.drawings}
        value={this.state.drawings}
        forceValue
        onChange={this._onSketchChange}
        tool={this.state.tool}
      />

      <div className="card bg-secondary float-left" style={{ maxWidth: '40rem' }}>
        <div className="card-header" style={{ paddingBottom: 5, paddingTop: 5 }}>Tools</div>
        <div className="card-body" style={{ paddingBottom: 14 }}>
          <div>
            <button className="btn btn-primary mr-2" value={Tools.Select} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Select</button>
            <button className="btn btn-primary mr-2" value={Tools.Pan} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Pan</button>
            <button className="btn btn-primary mr-2" value={Tools.Pencil} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Pencil</button>
            <button className="btn btn-primary mr-2" value={Tools.Line} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Line</button>
            <button className="btn btn-primary mr-2" value={Tools.Rectangle} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Rectangle</button>
            <button className="btn btn-primary" value={Tools.Circle} onMouseDown={this._selectTool} onMouseUp={this._selectTool}>Circle</button>
          </div>
          <div className="mt-1">
            <fieldset className="form-group mb-0 pb-0">
              <input type="range" className="custom-range" id="customRange1" value={this.state.lineWidth} disabled={true} />
            </fieldset>
          </div>
          <div>
            <input className="form-control mt-1 mb-1" label="Text" placeholder="Type text or paste an image link"
              onChange={e => this.setState({ text: e.target.value })} value={this.state.text}></input>
            <button className="btn btn-primary mr-2" onClick={this._addText}>Add Text</button>
            <button className="btn btn-primary mr-2" onClick={e => this._sketch.addImg(this.state.text)}>Add Image</button>
            <button className="btn btn-primary mr-2" onClick={e => this._importKanban()}>Import Kanban</button>
            <button className="btn btn-primary mr-2" disabled={!this.state.kanbanLink} onClick={e => this.props.history.push('/boards/' + this.state.kanbanLink)}>Go To Kanban</button>
          </div>
        </div>
      </div>

      <div className="card bg-secondary float-right">
        <div className="card-body" style={{ paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0 }}>
          <ChromePicker color={this.state.lineColor} onChangeComplete={this._handleChangeColor} />
        </div>
      </div>

      <div className="card bg-secondary">
        <div className="card-header" style={{ paddingTop: 5, paddingBottom: 5 }}>Instructions</div>
        <div className="card-body" style={{ paddingBottom: 2, paddingTop: 2, minWidth: 277 }}>
          <p className="mb-0 pb-0">ALT+1 to ALT+6 - Tools <br />
          CTRL+Z/Y - Undo/Redo <br />
          Backspace - Remove selected element<br />
          ALT+C/V - Copy/Paste selected element<br />
          Up/Down - Line width<br />
          To add an image paste the link in the textbox<br />
          Don't forget to save!</p>
        </div>
      </div>

      <ChatWindow boardId={this.currentWbId} room='drawings/' boardTitle={this.curretnWbTitle} />
    </div>
    );
  }
}

export default Whiteboard;