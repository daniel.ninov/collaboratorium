import React, { useState, useEffect } from 'react';
import WhiteboardLink from '../WhiteboardLink/WhiteboardLink';
import * as firebase from 'firebase/app';
import 'firebase/database';
import CreateWhiteboard from '../CreateWhiteboard/CreateWhiteboard';

const PublicWhiteboards = () => {

  const [publicBoards, setPublicBoards] = useState([]);
  const database = firebase.database();

  useEffect(() => {
    
    const countersRef = database.ref('whiteboardsCounters');

    const boardsRef = database.ref('drawings');
    countersRef.on('value', function (snapshot) {
      boardsRef.once('value', function (snapshot) {
        if (!!snapshot.val()){
          const allBoards = Object.values(snapshot.val());
          const onlyPublicBoards = allBoards.filter(elem => elem.details?.isPublic);

          setPublicBoards(onlyPublicBoards);
        }
      });
    });

    return () => {
      countersRef.off();
      boardsRef.off();
    };
  }, [database]);

  return (
    <div>
      {publicBoards.map(elem => <WhiteboardLink key={elem.details.id} whiteboard={elem.details} />)}
      <CreateWhiteboard publicity={true} />
    </div>
  )


};

export default PublicWhiteboards;