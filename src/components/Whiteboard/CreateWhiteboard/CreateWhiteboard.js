import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import * as firebase from 'firebase/app';
import { useAuthState } from 'react-firebase-hooks/auth';
import 'firebase/auth';
import * as shortid from 'shortid';
import { Tools } from 'react-sketch';
import WhiteboardCounterInc from '../../../utils/WhiteboardCounterInc';

const CreateWhiteboard = ({ publicity = false }) => {
  const [showModule, setShowModule] = useState(false);
  const [createBoardForm, setCreateBoardForm] = useState({
    title: {
      value: '',
      touched: false,
      valid: true,
    },
    isPublic: {
      value: publicity,
      touched: false,
      valid: true,
    }
  });
  const database = firebase.database();
  const [user] = useAuthState(firebase.auth());

  const currentLoggedUser = {
    id: user.uid,
    username: user.providerData[0].displayName,
  };

  const updateForm = (prop, value) => {
    setCreateBoardForm({
      ...createBoardForm,
      [prop]: {
        value: value,
        touched: true,
        valid: formValidators[prop].reduce((acc, validatorFn) => acc && validatorFn(value), true)
      }
    });
  };

  const formValidators = {
    title: [
      value => value && value.length >= 3,
      value => value && value.length <= 30,
    ],
    isPublic: []
  };

  const validateForm = () => !Object
    .keys(createBoardForm)
    .reduce((isValid, prop) => {
      if (prop === 'isPublic') return isValid && true;

      return isValid && createBoardForm[prop].valid && createBoardForm[prop].touched;
    },
      true);

  const showModuleButton = () => {

    return (
      <button className="btn btn-primary" onClick={() => setShowModule(!showModule)}>{showModule ? 'Hide Create Whiteboard module' : 'Show Create Whiteboard module'}</button>
    )
  };

  const handleSubmit = async () => {
    const newBoard = {
      board: {
        lineWidth: 10,
        lineColor: 'black',
        fillColor: '#68CCCA',
        backgroundColor: 'transparent',
        shadowWidth: 0,
        shadowOffset: 0,
        tool: Tools.Pencil,
        enableRemoveSelected: false,
        fillWithColor: false,
        fillWithBackgroundColor: false,
        drawings: '',
        canUndo: false,
        canRedo: false,
        controlledSize: false,
        sketchWidth: 600,
        sketchHeight: 600,
        stretched: true,
        stretchedX: false,
        stretchedY: false,
        originX: 'left',
        originY: 'top',
        expandTools: false,
        expandControls: false,
        expandColors: false,
        expandBack: false,
        expandImages: false,
        expandControlled: false,
        text: '',
        enableCopyPaste: false,
      },
      details: {
        id: shortid.generate(),
        title: createBoardForm.title.value,
        isPublic: createBoardForm.isPublic.value,
        owner: currentLoggedUser.username,
        ownerId: currentLoggedUser.id,
        allowedIds: [currentLoggedUser.id],
      },
    };

    await database.ref('drawings/' + newBoard.details.id).set(newBoard)
    .then(async res => {
      await WhiteboardCounterInc(newBoard.details.isPublic);
    }).catch(e => console.log(e.message));

    setShowModule(false);
  };

  const createBoardFormDisplay = () => {
    return (
      <Form>
        <Form.Group controlId='createBoardFrom.title'>
          <Form.Label style={{ marginTop: 10 }} >Whiteboard Title</Form.Label>
          <Form.Control style={{ maxWidth: '40rem' }} type='text' placeholder='Whiteboard Title' value={createBoardForm.title.value} onChange={e => updateForm('title', e.target.value)} />
        </Form.Group>
        <Form.Check type='checkbox' label='is Public' checked={createBoardForm.isPublic.value} onChange={() => updateForm('isPublic', !createBoardForm.isPublic.value)} />
        <Button style={{ marginTop: 10 }} disabled={validateForm()} onClick={handleSubmit}>Submit</Button>
      </Form>
    )
  }

  return (
    <div>
      {showModuleButton()}
      {showModule ?
        createBoardFormDisplay() :
        null}
    </div>
  );
}

export default withRouter(CreateWhiteboard);