import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Form, Col, Button } from 'react-bootstrap';
import * as firebase from 'firebase/app';
import { useAuthState } from 'react-firebase-hooks/auth';
import 'firebase/auth';
import 'firebase/database';
import WhiteboardCounterDec from '../../../utils/WhiteboardCounterDec';
import WhiteboardCounterInc from '../../../utils/WhiteboardCounterInc';
import WhiteboardInvite from '../WhiteboardInvite/WhiteboardInvite';


const WhiteboardLink = ({ whiteboard, history }) => {
  const database = firebase.database();
  const [title, setTitle] = useState({
    value: whiteboard.title,
    valid: true
  });
  const [renameMode, setRenameMode] = useState(false);

  const [boardPublicity, setBoardPublicity] = useState(false);

  const [user] = useAuthState(firebase.auth());

  const currentLoggedUser = {
    id: user.uid,
    username: user.providerData[0].displayName,
  };

  useEffect(() => {
    database.ref('drawings/' + whiteboard.id + '/details').on('value', snapshot => {
      if (!!snapshot.val()) {
        setTitle({ valid: true, value: snapshot.val().title });
        setBoardPublicity(snapshot.val().isPublic);
      }

    });

    return () => {
      database.ref('drawings/' + whiteboard.id + '/details').off();
    };
  }, [whiteboard.id, database]);

  const titleValidators = [
    value => value && value.length >= 2,
    value => value && value.length <= 30,
  ];

  const updateTitle = (newValue) => {
    setTitle({ value: newValue, valid: titleValidators.reduce((isValid, validatorFn) => isValid && validatorFn(newValue), true) });
  }

  const handleDelete = (boardId = whiteboard.id) => {
    database.ref('drawings/' + boardId).remove();
    WhiteboardCounterDec(boardPublicity);
  };

  const handleRename = (boardId = whiteboard.id) => {
    database.ref('drawings/' + boardId + '/details').update({ ...whiteboard, title: title.value });
    setRenameMode(false);
  }

  const togglePublicity = () => {
    WhiteboardCounterDec(boardPublicity);
    WhiteboardCounterInc(!boardPublicity);
    database.ref('drawings/' + whiteboard.id + '/details').update({ whiteboard, isPublic: !whiteboard.isPublic });
  }

  const renameModule = () => {
    return (
      <>
        <Col xs='auto'>
          <Form.Control type='text' value={title.value} onChange={e => updateTitle(e.target.value)} />
        </Col>
        <Col xs='auto'>
          <Button disabled={!title.valid} onClick={() => handleRename()}>√</Button>
        </Col>
        <Col xs='auto'>
          <Button onClick={() => setRenameMode(false)}>✗</Button>
        </Col>
      </>

    )
  }

  const handleOpenBoard = () => {
    history.push('/draw/' + whiteboard.id);
  }

  return (
    <Form>
      <Form.Group>
        <Form.Row>
          <Form.Label column sm='2'>Title</Form.Label>

          {renameMode ?
            renameModule() :
            <Col sm='10'>
              <Form.Control plaintext readOnly value={title.value} onChange={e => updateTitle(e.target.value)} onClick={() => setRenameMode(!renameMode)} />

            </Col>
          }
        </Form.Row>
        <Form.Row>
          <Form.Label column sm='2'>Owner</Form.Label>
          <Col ms='10'>
            <Form.Control plaintext readOnly defaultValue={whiteboard.owner} />
          </Col>
        </Form.Row>
        <Form.Row>
          <Form.Label column sm='2'>Actions</Form.Label>
          {currentLoggedUser.id === whiteboard.ownerId ?
            <>
              <Col xs='auto'>
                <Button onClick={() => setRenameMode(!renameMode)}>{renameMode ? 'Cancel' : 'Rename'}</Button>
              </Col>
              <Col xs='auto'>
                <Button onClick={() => handleDelete()}>Delete</Button>
              </Col>
              <Col xs='auto'>
                <Button onClick={togglePublicity}>Toggle publicity</Button>
              </Col>
              <Col xs='auto'>
                <WhiteboardInvite whiteboardId={whiteboard.id} />
              </Col>
            </> : null
          }
          <Col xs='auto'>
            <Button onClick={() => handleOpenBoard()}>View</Button>
          </Col>
        </Form.Row>
      </Form.Group>
    </Form>
  )
}

export default withRouter(WhiteboardLink);